
# TSI Reporting Service

Service specifically designed to produce reports for TSI.

# Setup

## Local Dev

### Environment

Install nodeJS, the easiest way is to use NVM (Node Version Manager):

* (NVM)[https://github.com/creationix/nvm]

This will install node and npm (Node Package Manager).

### Dependencies

From within the project folder:

```
npm install
cd public
npm install
```

## Production

Install dependencies:

```
npm install --production
cd public
npm install
```

# Running

## Starting

Starting application

```
npm start
```

Local URL:

* (http://localhost:3000)[http://localhost:3000]

## Tests

```
npm test
```

# Serverless delivery

(Article)[https://medium.freecodecamp.org/express-js-and-aws-lambda-a-serverless-love-story-7c77ba0eaa35]