var express = require('express');
var router = express.Router();
var request = require('request');

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

/* GET home page. */
router.get('/', function(req, res, next) {
  request.get({'uri':'https://dev.thesocialindex.com/tsi/api/getallinfographic/599','json': true},function(err,resp,body){
    res.render('index', { title: 'Kevin Michaels', 'report': JSON.stringify(body) });
  });
});

router.get('/professional', function(req, res, next) {
  // res.render('professional', { title: 'Professional' });
  request.get({'uri':'https://dev.thesocialindex.com/tsi/api/getallinfographic/599','json': true},function(err,resp,body){
    res.render('professional', { title: 'Kevin Michaels', 'report': JSON.stringify(body) });
  });
});

router.get('/enterprise', function(req, res, next) {
  // res.render('enterprise', { title: 'Enterprise' });
  request.get({'uri':'https://dev.thesocialindex.com/tsi/api/getallinfographic/599','json': true},function(err,resp,body){
    res.render('enterprise', { title: 'Kevin Michaels', 'report': JSON.stringify(body) });
  });
});

module.exports = router;
