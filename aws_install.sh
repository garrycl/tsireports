#!/usr/bin/env bash
set -e

# update instance
sudo apt-get update

# add source
wget -qO- https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get update

# update instance
sudo apt-get install -y nodejs

# install pm2 module globaly
npm install -g pm2
pm2 update
chown ubuntu:ubuntu /home/ubuntu/.pm2/rpc.sock /home/ubuntu/.pm2/pub.sock
rm -r /home/ubuntu/report-service/*