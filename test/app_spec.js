// test/app_spec.js
// ref: https://groundberry.github.io/development/2016/12/10/testing-express-with-mocha-and-chai.html

var chai = require('chai');
var chaiHttp = require('chai-http');
var app = require('../app');

var expect = chai.expect;

chai.use(chaiHttp);

describe('App', function() {
  // Ensure that pages work
  describe('GET /', function() {
    it('responds with status 200', function(done) {
      chai.request(app)
        .get('/')
        .end(function(err, res) {
          expect(res).to.have.status(200);
          done();
        });
    });
  });
});